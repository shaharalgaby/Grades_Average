
package grades;
import java.io.Serializable;
import javax.persistence.*;

@Entity
public class Course implements Serializable {
    @Id
    private int id;
    private String name;
    private double grade;
    private String courseLevel;
    private int points;
    private boolean countInAverage;
    
    public Course(){}
    public Course(int id,String name,double grade,String level,int points,boolean cia)
    {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.courseLevel = level;
        this.points = points;
        this.countInAverage = cia;
    }

    public String getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(String courseLevel) {
        this.courseLevel = courseLevel;
    }

    public boolean isCountInAverage() {
        return countInAverage;
    }

    public void setCountInAverage(boolean countInAverage) {
        this.countInAverage = countInAverage;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTheName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public String getLevel() {
        return courseLevel;
    }

    public void setLevel(String level) {
        this.courseLevel = level;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
    public String toString()
    {
        return id+" "+name+" "+grade+" "+courseLevel+"  "+points+" ";
    }
}
