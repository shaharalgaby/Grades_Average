/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grades;
    import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
    import javax.persistence.*;
/**
 *
 * @author shahar
 */
@Entity
public class MetaData {
    @Id@GeneratedValue
    private int id;
    private boolean scientific;
    @OneToMany
    private List<Course> list;
    
    public MetaData(){}
    public MetaData(boolean s, List<Course> l)
    {
        this.scientific = s;
        this.list = l;
    }
    public boolean isScientific()
    {
        return scientific;
    }
    public void setScientific(boolean b)
    {
        scientific = b;
    }

    public List<Course> getList() {
        return list;
    }

    public void setList(List<Course> list) {
        list.clear();
        for(Course c: list)
          list.add(new Course(c.getId(),c.getTheName(),c.getGrade(),c.getLevel(),c.getPoints(),c.isCountInAverage()));
    }
}
