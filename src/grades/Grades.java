
package grades;

//import com.sun.xml.internal.ws.dump.LoggingDumpTube;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

public class Grades extends javax.swing.JFrame {
    EntityManagerFactory emf;
    EntityManager em;
    final int btnSize = 170;
    final int btnPerRow = 4;
    boolean scientific ;
    private MetaData data;
    
    public Grades() {
        initComponents();
                
        //Scrolling speed
        scrollPane.getVerticalScrollBar().setUnitIncrement(20);
        scientific = checkBox.isSelected();
                
        emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        //em.getTransaction().commit();
        
        data = em.find(MetaData.class, 1);
        if(data==null)
        {
            MetaData d = new MetaData(false,new ArrayList<>());
            em.persist(d);
            em.getTransaction().commit();
            data = d;
        }
        if( data.isScientific())
        {
            checkBox.setSelected(true);
        }
        addCourses();
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        scrollPane = new javax.swing.JScrollPane();
        coursesPanel = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        addBtn = new javax.swing.JButton();
        scoreLbl = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        checkBox = new javax.swing.JCheckBox();
        sveBtn = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setOpaque(false);

        scrollPane.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        coursesPanel.setOpaque(false);
        coursesPanel.setLayout(new javax.swing.BoxLayout(coursesPanel, javax.swing.BoxLayout.Y_AXIS));
        scrollPane.setViewportView(coursesPanel);

        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel3.setOpaque(false);

        addBtn.setFont(new java.awt.Font("Tekton Pro", 0, 48)); // NOI18N
        addBtn.setText("+");
        addBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addBtnMouseClicked(evt);
            }
        });

        scoreLbl.setFont(new java.awt.Font("Tekton Pro", 0, 48)); // NOI18N
        scoreLbl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        scoreLbl.setText("99.9");

        jLabel1.setFont(new java.awt.Font("Tekton Pro", 0, 36)); // NOI18N
        jLabel1.setText("Average:");

        checkBox.setText("Scientific degree?");
        checkBox.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                checkBoxMouseClicked(evt);
            }
        });
        checkBox.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                checkBoxPropertyChange(evt);
            }
        });

        sveBtn.setText("Make Default");
        sveBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sveBtnMouseClicked(evt);
            }
        });

        jButton2.setText("Restore");
        jButton2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButton2KeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scoreLbl, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sveBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, 132, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(addBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(addBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(checkBox)
                            .addComponent(sveBtn)
                            .addComponent(jButton2)))
                    .addComponent(scoreLbl, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPane)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void addCourses()
    {      
        coursesPanel.removeAll();
        coursesPanel.revalidate();
        coursesPanel.repaint();
        coursesPanel.setOpaque(true);
                
        coursesPanel.setOpaque(true);
        scrollPane.setOpaque(true);
        coursesPanel.setForeground(Color.red);
        scrollPane.setForeground(Color.red);
        //coursesPanel.setBackground(Color.red);
        //scrollPane.setBackground(Color.red);
        
        emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        
        //JFrame backgroud set
        //this.getContentPane().setBackground(Color.red);
          
        int width = coursesPanel.getWidth();     

        //get The products list from DB
        Query q1 = em.createQuery("SELECT c FROM Course c",Course.class);
        List<Course> courses = q1.getResultList();
        int size = courses.size();
        
        for(int i=0;i<size;i++)
        {
            JPanel p1 = new JPanel(new FlowLayout());
            int count = 0;
            //4 buttons in one line
            while(count<btnPerRow && i<size){
                Course c = courses.get(i);
                
                //Set button properties
                JButton btn = new JButton("");
                //BoxLayout layout = new BoxLayout(btn,BoxLayout.Y_AXIS);
                GridLayout layout = new GridLayout(4,1,0,0);
                btn.setPreferredSize(new Dimension(btnSize,btnSize));
                btn.setLayout(layout);

                JLabel nameLbl = new JLabel(c.getTheName());
                JLabel idLbl = new JLabel(String.valueOf(c.getId()));
                JLabel scoreLbl = new JLabel(String.valueOf(c.getGrade()));
                nameLbl.setFont(new Font("David",10,20));
                idLbl.setFont(new Font("David",10,14));
                scoreLbl.setFont(new Font("David",10,20));
                      
                nameLbl.setHorizontalAlignment(JLabel.CENTER);
                idLbl.setHorizontalAlignment(JLabel.CENTER);
                scoreLbl.setHorizontalAlignment(JLabel.CENTER);

                btn.add(idLbl);
                btn.add(nameLbl);
                btn.add(scoreLbl);
                
                //TO delete a course
                btn.addMouseListener(new MouseAdapter(){
                    @Override
                    public void mouseClicked(MouseEvent e) 
                    {
                        if(e.getButton()==MouseEvent.BUTTON3)
                        {
                            JPopupMenu menu = new JPopupMenu();
                            JMenuItem item = new JMenuItem("Delete");
                            menu.add(item);
                            
                            menu.show(e.getComponent(), e.getX(), e.getY());
                            item.addActionListener(new ActionListener(){
                                public void actionPerformed(ActionEvent e)
                                {
                                    removeCourse(c.getId());
                                }
                            });
                        }
                    }
                });
                btn.addActionListener(new ActionListener(){
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        
                        CoursePanel coursePanel = new CoursePanel(c);
                        int res = JOptionPane.showConfirmDialog(null, coursePanel, "",
                        JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);
                        if(res == JOptionPane.OK_OPTION)
                            updateResult(coursePanel);
                    }

                });
                p1.add(btn);
                i++;
                count++;
                coursesPanel.add(p1);
            }
            i--;
            coursesPanel.add(p1);
        }     
        
        //Calculating the sum
        double sum = 0;
        int nakaz = 0;
        for(Course course: courses)
        {
            if(course.isCountInAverage())
            {
                if((checkBox.isSelected() && course.getLevel().equals("Advanced"))||course.getLevel().equals("Seminar Advanced"))
                {
                    sum += course.getGrade()*course.getPoints()*1.5;
                    nakaz += course.getPoints()*1.5;
                }
                else
                {
                    sum += course.getGrade()*course.getPoints();
                    nakaz += course.getPoints();
                }
            }
        }
        if(nakaz!=0)
        {
            Double average = (Double)(sum/nakaz);
            scoreLbl.setText(new DecimalFormat("00.00").format(average));
        } 
        em.close();
        emf.close();
    }
    private void removeCourse(int id)
    {
        emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        
        Course c = em.find(Course.class, id);
        
        em.remove(c);
        
        em.getTransaction().commit();
        
        addCourses();
    }
    private void addBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addBtnMouseClicked
        CoursePanel coursePanel = new CoursePanel();
        int res = JOptionPane.showConfirmDialog(this, coursePanel, "",
                JOptionPane.OK_CANCEL_OPTION,JOptionPane.PLAIN_MESSAGE);
        
        if(res == JOptionPane.OK_OPTION)
            saveResult(coursePanel);
        
    }//GEN-LAST:event_addBtnMouseClicked

    private void checkBoxPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_checkBoxPropertyChange
    }//GEN-LAST:event_checkBoxPropertyChange

    private void checkBoxMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_checkBoxMouseClicked
        emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        
        MetaData d = em.find(MetaData.class, 1);
                
        if(checkBox.isSelected())
            d.setScientific(true);     
        else
            d.setScientific(false);
          
        em.getTransaction().commit();   
        addCourses();
    }//GEN-LAST:event_checkBoxMouseClicked

    private void sveBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sveBtnMouseClicked
        emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        MetaData d = em.find(MetaData.class, 1);
        
        Query q1 = em.createQuery("SELECT c FROM Course c",Course.class);
        List<Course> result = q1.getResultList();
        ArrayList<Course> newList = new ArrayList<>();
        for(Course c: result)
            newList.add(new Course(c.getId(),c.getTheName(),c.getGrade(),c.getLevel(),c.getPoints(),c.isCountInAverage()));
            
        d.setList(newList);
        
        em.getTransaction().commit();
        em.close();        
        emf.close();
        
    }//GEN-LAST:event_sveBtnMouseClicked

    private void jButton2KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButton2KeyPressed
        emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
        em = emf.createEntityManager();
        em.getTransaction().begin();
        MetaData d = em.find(MetaData.class, 1);
        List<Course> list = d.getList();
        
        
    }//GEN-LAST:event_jButton2KeyPressed
    public void updateResult(CoursePanel coursePanel)
    {
        boolean problem = false;
        String grade = coursePanel.getGrade();
        String name = coursePanel.getTheName();
        String level = coursePanel.getLevel();
        String id = coursePanel.getId();
        int points = coursePanel.getPoints();
        boolean cia = coursePanel.countInAverage();
        double gradeInt = -1;
        try{
            gradeInt = Double.parseDouble(grade);
        }catch(NumberFormatException e)
        {
            //handle exception
            JOptionPane.showMessageDialog(rootPane, "Wrong grade format");
            problem = true;
        }
        if(!problem)
        {
            emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
            em = emf.createEntityManager();
            em.getTransaction().begin();

            int theId = Integer.parseInt(id);

            Course course = em.find(Course.class, theId);

            course.setGrade(gradeInt);
            course.setName(name);
            course.setLevel(level);
            course.setId(theId);
            course.setPoints(points);
            course.setCountInAverage(cia);

            em.getTransaction().commit();

            addCourses();
        }
    }
    public void saveResult(CoursePanel coursePanel)
    {
        boolean problem = false;
        String grade = coursePanel.getGrade();
        String name = coursePanel.getTheName();
        String level = coursePanel.getLevel();
        String id = coursePanel.getId();
        boolean cia = coursePanel.countInAverage();
        int points = coursePanel.getPoints();
        double gradeInt = -1;
        try{
            int idInt = Integer.parseInt(id);
        }catch(NumberFormatException e)
        {
            //handle exception
            JOptionPane.showMessageDialog(rootPane, "Wrong course number format");
            problem = true;
        }
        try{
            gradeInt = Double.parseDouble(grade);
        }catch(NumberFormatException e)
        {
            //handle exception
            JOptionPane.showMessageDialog(rootPane, "Grade format");
            problem = true;
        }
        if(!problem)
        {
            Course course = new Course(Integer.parseInt(id), name,
                    Double.parseDouble(grade), level, points,cia);

            emf = Persistence.createEntityManagerFactory("$objectdb/db/Grades.odb");
            em = emf.createEntityManager();
            em.getTransaction().begin();
            try{
                em.persist(course);
                em.getTransaction().commit();
            }
            catch(IllegalArgumentException e ){
                System.out.println("Problem");
            }
            em.close();
            emf.close();

            addCourses();
        }
    }
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Grades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Grades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Grades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Grades.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Grades().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addBtn;
    private javax.swing.JCheckBox checkBox;
    private javax.swing.JPanel coursesPanel;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel scoreLbl;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JButton sveBtn;
    // End of variables declaration//GEN-END:variables
}
